package pl.kamilmielnik.fluid.display.derivable;

public interface RepaintListener {

	void requestRepaint();

}
