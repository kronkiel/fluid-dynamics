package pl.kamilmielnik.fluid.display;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

import pl.kamilmielnik.fluid.configuration.derivable.Configuration;
import pl.kamilmielnik.fluid.display.colors.ColorStrategy;
import pl.kamilmielnik.fluid.display.derivable.RepaintListener;
import pl.kamilmielnik.fluid.display.filters.BlurFilter;
import pl.kamilmielnik.fluid.display.filters.ImageFilter;
import pl.kamilmielnik.fluid.input.DefaultSimulationInputListener;
import pl.kamilmielnik.fluid.model.Simulation;

@SuppressWarnings("serial")
public class SimulationCanvas extends Canvas implements RepaintListener {
	
	protected Configuration configuration;
	protected Simulation simulation;
	protected ColorStrategy colorStrategy;
	protected Graphics bufferGraphics;
	protected Image offscreen;

	public SimulationCanvas(Configuration configuration, DefaultSimulationInputListener inputListener) {
		this.configuration = configuration;
		this.simulation = null;
		this.colorStrategy = configuration.getColorStrategy();
		this.bufferGraphics = null;
		this.offscreen = null;
		this.setSize(configuration.getWidth(), configuration.getHeight());
		this.addMouseListener(inputListener);
		this.addMouseMotionListener(inputListener);
		this.addKeyListener(inputListener);
	}

	public void setSimulation(Simulation simulation) {
		if (this.simulation != null) simulation.removeListener(this);
		this.simulation = simulation;
		this.simulation.addListener(this);
	}

	protected void paintDensity(Graphics graphics) {
		float[][] density = simulation.getDensity();
		int length = configuration.getCellLength();
		int width = simulation.getConfiguration().getGridWidth();
		int height = simulation.getConfiguration().getGridHeight();
		graphics.setColor(Color.white);
		graphics.clearRect(0, 0, width, height);
		colorStrategy = configuration.getColorStrategy();
		for (int i = 1; i <= width; ++i) {
			for (int j = 1; j <= height; ++j) {
				graphics.setColor(colorStrategy.getColor(density, i, j));
				graphics.fillRect((int) ((i - 1) * length), (int) ((j - 1) * length), length, length);
			}
		}
	}

	@Override
	public void paint(Graphics graphics) {
		offscreen = createImage(configuration.getWidth(), configuration.getHeight());
		bufferGraphics = offscreen.getGraphics();
		paintDensity(bufferGraphics);

		final ImageFilter d = new BlurFilter(3);
		offscreen = d.filter((BufferedImage) offscreen);

		graphics.drawImage(offscreen, 0, 0, this);
	}

	public void requestRepaint() {
		paint(getGraphics());
	}

}
