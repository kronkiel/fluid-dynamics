package pl.kamilmielnik.fluid.display.filters;

import java.awt.image.BufferedImage;

public abstract class ImageFilter {

	public abstract BufferedImage filter(BufferedImage image);

}
