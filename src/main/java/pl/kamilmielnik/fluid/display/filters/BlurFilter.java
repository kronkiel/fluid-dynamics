package pl.kamilmielnik.fluid.display.filters;

import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;

public class BlurFilter extends ImageFilter {

	protected int blur;

	public BlurFilter(int blur) {
		this.blur = blur;
	}

	public BufferedImage filter(BufferedImage image) {
		int blurSquared = blur * blur;
		float[] matrix = new float[blurSquared];
		for (int i = 0; i < blurSquared; ++i) {
			matrix[i] = 1.0f / blurSquared;
		}
		BufferedImage newImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_RGB);
		BufferedImageOp op = new ConvolveOp(new Kernel(blur, blur, matrix));
		BufferedImage blurredImage = op.filter(image, newImage);
		return blurredImage;
	}
}
