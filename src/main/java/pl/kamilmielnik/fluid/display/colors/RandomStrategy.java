package pl.kamilmielnik.fluid.display.colors;

import java.awt.Color;
import java.util.Random;

public class RandomStrategy extends ColorStrategy {

	protected float hue;

	public RandomStrategy() {
		this.hue = getRandom();
	}

	protected static float getRandom() {
		Random random = new Random(System.currentTimeMillis());
		random.nextFloat();
		return random.nextFloat();
	}

	@Override
	public Color getColor(float[][] density, int i, int j) {
		float d = density[i][j];
		return Color.getHSBColor(hue, 0.75f, d);
	}

}
