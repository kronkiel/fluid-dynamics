package pl.kamilmielnik.fluid.display.colors;

import java.awt.Color;

public class BlackAndWhiteStrategy extends ColorStrategy{

	@Override
	public Color getColor(float[][] density, int i, int j) {
		return Color.getHSBColor(0, 0, 1 - density[i][j]);
	}

}
