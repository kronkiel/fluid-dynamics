package pl.kamilmielnik.fluid.display.colors;

import java.awt.Color;

public class RainbowStrategy extends ColorStrategy {

	@Override
	public Color getColor(float[][] density, int i, int j) {
		return Color.getHSBColor((1 - density[i][j]) * 0.9f, 1.0f, 0.7f);
	}

}
