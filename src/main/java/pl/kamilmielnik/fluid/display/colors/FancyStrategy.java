package pl.kamilmielnik.fluid.display.colors;

import java.awt.Color;

public class FancyStrategy extends ColorStrategy {

	@Override
	public Color getColor(float[][] density, int i, int j) {
		float d = density[i][j];
		return Color.getHSBColor(0.5f, 0.75f, d);
	}

}
