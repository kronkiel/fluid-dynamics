package pl.kamilmielnik.fluid.display.colors;

import java.awt.Color;

public abstract class ColorStrategy {

	public abstract Color getColor(float[][] density, int i, int j);

}
