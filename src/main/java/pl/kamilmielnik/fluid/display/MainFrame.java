package pl.kamilmielnik.fluid.display;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import pl.kamilmielnik.fluid.configuration.derivable.Configuration;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {

	protected Canvas panel;
	protected Configuration configuration;

	public MainFrame(Canvas panel, Configuration configuration) {
		this.panel = panel;
		this.configuration = configuration;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.add(panel);
		this.setSize(configuration.getWidth(), configuration.getHeight());
		this.setTitle(configuration.getWindowTitle());
		centerFrame(this);
	}

	protected static void centerFrame(JFrame frame) {
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
		int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
		frame.setLocation(x, y);
	}

}
