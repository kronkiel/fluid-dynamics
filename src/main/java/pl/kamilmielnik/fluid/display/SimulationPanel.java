package pl.kamilmielnik.fluid.display;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

import pl.kamilmielnik.fluid.configuration.derivable.Configuration;
import pl.kamilmielnik.fluid.display.colors.ColorStrategy;
import pl.kamilmielnik.fluid.display.derivable.RepaintListener;
import pl.kamilmielnik.fluid.input.DefaultSimulationInputListener;
import pl.kamilmielnik.fluid.model.Simulation;

@SuppressWarnings("serial")
public class SimulationPanel extends JPanel implements RepaintListener {

	protected Configuration configuration;
	protected Simulation simulation;
	protected ColorStrategy colorStrategy;
	protected Graphics bufferGraphics;
	protected Image offscreen;

	public SimulationPanel(Configuration configuration, DefaultSimulationInputListener inputListener) {
		this.configuration = configuration;
		this.simulation = null;
		this.colorStrategy = configuration.getColorStrategy();
		this.bufferGraphics = null;
		this.offscreen = null;
		this.setSize(configuration.getWidth(), configuration.getHeight());
		this.addMouseListener(inputListener);
		this.addMouseMotionListener(inputListener);
	}

	public void setSimulation(Simulation simulation) {
		if (this.simulation != null) simulation.removeListener(this);
		this.simulation = simulation;
		this.simulation.addListener(this);
	}

	protected void paintDensity(Graphics graphics) {
		float[][] density = simulation.getDensity();
		int length = configuration.getCellLength();
		int width = simulation.getConfiguration().getGridWidth();
		int height = simulation.getConfiguration().getGridHeight();
		for (int i = 1; i <= width; ++i) {
			for (int j = 1; j <= height; ++j) {
				graphics.setColor(colorStrategy.getColor(density, i, j));
				graphics.fillRect((int) ((i - 1) * length), (int) ((j - 1) * length), length, length);
			}
		}
	}

	@Override
	public void paint(Graphics graphics) {
		offscreen = createImage(configuration.getWidth(), configuration.getHeight());
		bufferGraphics = offscreen.getGraphics();
		paintDensity(bufferGraphics);
		graphics.drawImage(offscreen, 0, 0, this);
	}

	public void requestRepaint() {
		paint(getGraphics());
	}

}
