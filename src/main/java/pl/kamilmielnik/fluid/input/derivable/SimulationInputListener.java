package pl.kamilmielnik.fluid.input.derivable;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import pl.kamilmielnik.fluid.configuration.derivable.Configuration;
import pl.kamilmielnik.fluid.model.Simulation;

public abstract class SimulationInputListener implements MouseMotionListener, MouseListener, KeyListener {

	protected Configuration configuration;
	protected Simulation simulation;

	public SimulationInputListener(Configuration configuration, Simulation simulation) {
		this.configuration = configuration;
		this.simulation = simulation;
	}

	public void mouseClicked(MouseEvent mouseEvent) {
	}

	public void mouseEntered(MouseEvent mouseEvent) {
	}

	public void mouseExited(MouseEvent mouseEvent) {
	}

	public void mouseReleased(MouseEvent mouseEvent) {
	}

	public void mouseMoved(MouseEvent mouseEvent) {
	}

	public void keyReleased(KeyEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

}
