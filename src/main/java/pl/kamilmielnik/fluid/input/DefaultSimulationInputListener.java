package pl.kamilmielnik.fluid.input;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import pl.kamilmielnik.fluid.configuration.derivable.Configuration;
import pl.kamilmielnik.fluid.display.colors.BlackAndWhiteStrategy;
import pl.kamilmielnik.fluid.display.colors.FancyStrategy;
import pl.kamilmielnik.fluid.display.colors.RainbowStrategy;
import pl.kamilmielnik.fluid.display.colors.RandomStrategy;
import pl.kamilmielnik.fluid.input.derivable.SimulationInputListener;
import pl.kamilmielnik.fluid.model.Simulation;

public class DefaultSimulationInputListener extends SimulationInputListener {

	protected int button;

	public DefaultSimulationInputListener(Configuration configuration, Simulation simulation) {
		super(configuration, simulation);
		this.button = MouseEvent.NOBUTTON;
	}

	public void mousePressed(MouseEvent mouseEvent) {
		button = mouseEvent.getButton();
		mouseDragged(mouseEvent);
	}

	public void mouseDragged(MouseEvent mouseEvent) {
		int x = mouseEvent.getX() / configuration.getCellLength() + 1;
		int y = mouseEvent.getY() / configuration.getCellLength() + 1;
		int brushSize = configuration.getBrushSize();
		int brushSizeSquared = brushSize * brushSize;

		switch (button) {
			case MouseEvent.BUTTON1:
				for (int i = x - brushSize; i <= x + brushSize; ++i) {
					for (int j = y - brushSize; j <= y + brushSize; ++j) {
						if ((x - i) * (x - i) + (y - j) * (y - j) < brushSizeSquared) {
							simulation.addDensity(i, j);
						}
					}
				}
				break;
			case MouseEvent.BUTTON3:
				for (int i = x - brushSize; i <= x + brushSize; ++i) {
					for (int j = y - brushSize; j <= y + brushSize; ++j) {
						if ((x - i) * (x - i) + (y - j) * (y - j) < brushSizeSquared) {
							simulation.removeDensity(i, j);
						}
					}
				}
				break;
			default:
				break;
		}
	}

	public void keyPressed(KeyEvent e) {
		switch (e.getKeyChar()) {
			case '[':
				configuration.setBrushSize(configuration.getBrushSize() - 1);
				break;
			case ']':
				configuration.setBrushSize(configuration.getBrushSize() + 1);
				break;
			case '1':
				configuration.setColorStrategy(new BlackAndWhiteStrategy());
				break;
			case '2':
				configuration.setColorStrategy(new RainbowStrategy());
				break;
			case '3':
				configuration.setColorStrategy(new FancyStrategy());
				break;
			case '4':
				configuration.setColorStrategy(new RandomStrategy());
				break;
		}
	}

}
