package pl.kamilmielnik.fluid.configuration.derivable;

public abstract class SimulationConfiguration {

	/***
	 * @return Amount of relaxation steps
	 */
	public abstract int getRelaxationSteps();

	/***
	 * @return Diffusion rate
	 */
	public abstract float getDiffusionRate();

	/***
	 * @return Time interval
	 */
	public abstract float getTimeInterval();

	/***
	 * @return Cell count in a single row
	 */
	public abstract int getGridWidth();
	
	/***
	 * @return Cell count in a single column
	 */
	public abstract int getGridHeight();

	/***
	 * @return Cell count in grid
	 */
	public int getGridSize() {
		int gridWidth = getGridWidth() + 2;
		int gridHeight = getGridHeight() + 2;
		return gridWidth * gridHeight;
	}

	/***
	 * @return Velocity
	 */
	public abstract float getVelocity();

	/***
	 * @return Density
	 */
	public abstract float getDensity();

	/***
	 * @return Step delay in millis
	 */
	public abstract long getDelay();

}
