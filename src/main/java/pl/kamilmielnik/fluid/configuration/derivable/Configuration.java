package pl.kamilmielnik.fluid.configuration.derivable;

import pl.kamilmielnik.fluid.display.colors.BlackAndWhiteStrategy;
import pl.kamilmielnik.fluid.display.colors.ColorStrategy;

public abstract class Configuration {

	protected int brushSize = 17;
	protected ColorStrategy colorStrategy = new BlackAndWhiteStrategy();

	public abstract String getWindowTitle();

	public abstract int getWidth();

	public abstract int getHeight();

	public int getBrushSize() {
		return brushSize;
	}

	public void setBrushSize(int brushSize) {
		this.brushSize = brushSize < 1 ? 1 : brushSize;
	}

	public abstract int getCellLength();

	public abstract int getFps();

	public ColorStrategy getColorStrategy() {
		return colorStrategy;
	}

	public void setColorStrategy(ColorStrategy colorStrategy) {
		this.colorStrategy = colorStrategy;
	}

}
