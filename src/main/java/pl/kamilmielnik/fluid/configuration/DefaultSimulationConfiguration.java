package pl.kamilmielnik.fluid.configuration;

import pl.kamilmielnik.fluid.configuration.derivable.SimulationConfiguration;

public class DefaultSimulationConfiguration extends SimulationConfiguration {

	@Override
	// 20 - Gauss-Seidel Relaxation
	public int getRelaxationSteps() {
		return 5;
	}

	@Override
	public float getDiffusionRate() {
		return 1.0f; // 1.0f is fine
	}

	@Override
	public float getTimeInterval() {
		return 1.0f;
	}

	@Override
	public int getGridWidth() {
		return 200;
	}

	@Override
	public int getGridHeight() {
		return 200;
	}

	@Override
	public float getVelocity() {
		return 5.0f;
	}

	@Override
	public float getDensity() {
		return 0.5f;
	}

	@Override
	public long getDelay() {
		return 20;
	}

}
