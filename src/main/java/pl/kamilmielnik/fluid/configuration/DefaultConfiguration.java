package pl.kamilmielnik.fluid.configuration;

import pl.kamilmielnik.fluid.configuration.derivable.Configuration;

public class DefaultConfiguration extends Configuration {

	@Override
	public String getWindowTitle() {
		return "Fluid dynamics - Kamil Mielnik";
	}

	@Override
	public int getWidth() {
		return 800;
	}

	@Override
	public int getHeight() {
		return 800;
	}

	@Override
	public int getCellLength() {
		return 4;
	}

	@Override
	public int getFps() {
		return 60;
	}

}
