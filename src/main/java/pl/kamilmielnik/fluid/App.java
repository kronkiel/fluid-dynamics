package pl.kamilmielnik.fluid;

import pl.kamilmielnik.fluid.configuration.DefaultConfiguration;
import pl.kamilmielnik.fluid.configuration.DefaultSimulationConfiguration;
import pl.kamilmielnik.fluid.configuration.derivable.Configuration;
import pl.kamilmielnik.fluid.configuration.derivable.SimulationConfiguration;
import pl.kamilmielnik.fluid.display.MainFrame;
import pl.kamilmielnik.fluid.display.SimulationCanvas;
import pl.kamilmielnik.fluid.input.DefaultSimulationInputListener;
import pl.kamilmielnik.fluid.model.ProperSimulation;
import pl.kamilmielnik.fluid.model.Simulation;

public class App {

	public static void main(String[] args) {
		Configuration configuration = new DefaultConfiguration();
		SimulationConfiguration simulationConfiguration = new DefaultSimulationConfiguration();
		Simulation simulation = new ProperSimulation(simulationConfiguration);
		DefaultSimulationInputListener inputListener = new DefaultSimulationInputListener(configuration, simulation);
		SimulationCanvas canvas = new SimulationCanvas(configuration, inputListener);
		canvas.setSimulation(simulation);
		MainFrame mainFrame = new MainFrame(canvas, configuration);
		mainFrame.setVisible(true);
		
		Thread simulationThread = new Thread(simulation);
		simulationThread.start();
		try {
			simulationThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
