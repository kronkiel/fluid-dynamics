package pl.kamilmielnik.fluid.model;

import java.util.ArrayList;
import java.util.List;

import pl.kamilmielnik.fluid.configuration.derivable.SimulationConfiguration;
import pl.kamilmielnik.fluid.display.derivable.RepaintListener;

public abstract class Simulation implements Runnable {

	protected SimulationConfiguration configuration;
	protected List<RepaintListener> listeners;

	public Simulation(SimulationConfiguration configuration) {
		this.configuration = configuration;
		this.listeners = new ArrayList<RepaintListener>();
	}

	public void addListener(RepaintListener listener) {
		listeners.add(listener);
	}

	public void removeListener(RepaintListener listener) {
		listeners.remove(listener);
	}

	public abstract float[][] getDensity();

	public SimulationConfiguration getConfiguration() {
		return configuration;
	}

	public abstract void addDensity(int x, int y);

	public abstract void removeDensity(int x, int y);

	protected void requestRepaint() {
		for (RepaintListener listener : listeners) {
			listener.requestRepaint();
		}
	}
}
